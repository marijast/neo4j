import { Food } from '../models/food.model';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { Picture } from '../models/picture.model';
import { HttpResponse } from '@angular/common/http';
import { Restaurant } from '../models/restaurant.model';


export interface Data {

     getAllFood() : Observable<Food[]>;
     getAllRestaurants() : Observable<Restaurant[]>;

     addFood(Username:string,Name: string, Type: string,Description:string,restaurantName:string,RestaurantCity:string,pictureURL:string): Observable<any>;
     addRestaurant(Name:string,City: string): Observable<any> ;
     register(Name: string, Username: string,Password:string): Observable<any>;
     getAllUsers() : Observable<User[]>;
     getAllPictures(): Observable<Picture[]>;
     login(username: string, password: string): Observable<any> ;
     getUserByUsername(username: string);
     getUserByUP(username: string,password:string): Observable<any>;
     getTastedFood(Username:string) :Observable<any>;
     getRecommendedFood(UsernameS,UsernameD):Observable<any>;
    getRestaurantByNameCity(Name:string,City:string): Observable<any>;
    getMenu(Name:string,City:string): Observable<any>;

    

    isUserLogged(): boolean ;
    getFoodByName(Name: string): Observable<any>;
    getRestaurantByCity(City: string): Observable<any>;
    getFoodById(Id:string): Observable<any>;
    getFoodByNameAndDescription(Name:string,Description:string): Observable<any>;
    getUser(Username:string): Observable<any>;
    addPicture(FoodName: string, PictureURL: string): Observable<any> ;
    link(FoodId: string, PictureURL: string): Observable<any> ;
    getTasters(Id:string) :Observable<any>;
    getPicturesOfMeal(Id:string):Observable<any>;
    vote(id:string,rate:number,username:string) :Observable<any>;
    getSimilarlyFood(name:string,type:string,desc:string): Observable<any>;

    deleteFood(id:string):Observable<any> ;










 
     
 }