import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { FoodComponent } from './components/food/food.component';
import { AddFoodComponent } from './components/add-food/add-food.component';
import { MealComponent } from './components/meal/meal.component';
import { CreateProfileComponent } from './components/create-profile/create-profile.component';
import { AllusersComponent } from './components/allusers/allusers.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { RecommendationComponent } from './components/recommendation/recommendation.component';
import { OneuserComponent } from './components/oneuser/oneuser.component';
import { TastedfoodComponent } from './components/tastedfood/tastedfood.component';
import { AddrestaurantComponent } from './components/addrestaurant/addrestaurant.component';
import { RestaurantsComponent } from './components/restaurants/restaurants.component';
import { RestaurantComponent } from './components/restaurant/restaurant.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'food', component: FoodComponent},
  {path: 'addFood', component: AddFoodComponent},
  {path: 'meal/:id', component: MealComponent},
  {path: 'restaurant/:Name/:City', component: RestaurantComponent},
  
  {path: 'user/:username', component: OneuserComponent},
  {path: 'users', component:AllusersComponent },
  {path: 'register', component: CreateProfileComponent},
  {path: 'logIn', component: LogInComponent},
  {path: 'userRecommendation' , component:RecommendationComponent},
  {path: 'myFood' , component:TastedfoodComponent},
  {path: 'addRestaurant' , component:AddrestaurantComponent},
  {path: 'restaurants' , component:RestaurantsComponent},
  

  {path: '**', component: HomeComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
