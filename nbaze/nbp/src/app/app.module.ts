import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { SliderModule } from 'angular-image-slider';
import { NgImageSliderModule } from 'ng-image-slider';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { FoodComponent } from './components/food/food.component';
import { HeaderComponent } from './components/header/header.component';
import { FoodInfoComponent } from './components/food-info/food-info.component';
import { FormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material';
import { AddFoodComponent } from './components/add-food/add-food.component';
import { FoodcardComponent } from './components/foodcard/foodcard.component';
import { MealComponent } from './components/meal/meal.component';
import { AllusersComponent } from './components/allusers/allusers.component';
import { CreateProfileComponent } from './components/create-profile/create-profile.component';
import { UserCardComponent } from './components/user-card/user-card.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { RecommendationComponent } from './components/recommendation/recommendation.component';
import { OneuserComponent } from './components/oneuser/oneuser.component';
import { TastedfoodComponent } from './components/tastedfood/tastedfood.component';
import { TastedcardComponent } from './components/tastedcard/tastedcard.component';
import { Header1Component } from './components/header1/header1.component';
import { AddrestaurantComponent } from './components/addrestaurant/addrestaurant.component';
import { RestaurantcardComponent } from './components/restaurantcard/restaurantcard.component';
import { RestaurantsComponent } from './components/restaurants/restaurants.component';
import { RestaurantComponent } from './components/restaurant/restaurant.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FoodComponent,
    HeaderComponent,
    FoodInfoComponent,
    AddFoodComponent,
    FoodcardComponent,
    MealComponent,
    AllusersComponent,
    CreateProfileComponent,
    UserCardComponent,
    LogInComponent,
    RecommendationComponent,
    OneuserComponent,
    TastedfoodComponent,
    TastedcardComponent,
    Header1Component,
    AddrestaurantComponent,
    RestaurantcardComponent,
    RestaurantsComponent,
    RestaurantComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule, 
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    SliderModule,
    NgImageSliderModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
