import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header1',
  templateUrl: './header1.component.html',
  styleUrls: ['./header1.component.scss']
})
export class Header1Component implements OnInit {
  // router: any;
 
   constructor(private dataService: DataService,private router: Router) { }
 
   ngOnInit() {
   }
 
   
 
   
 isLogged(): boolean {
   return this.dataService.isUserLogged();
 }
 
 logOut(): void {
   localStorage.removeItem("Username");
   localStorage.removeItem("Password");
   this.router.navigate(['home']);
 }
 }