import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DataService } from 'src/app/service/data.service';
import { Restaurant } from 'src/app/models/restaurant.model';
import { Food } from 'src/app/models/food.model';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss']
})
export class RestaurantComponent implements OnInit {

  
  restaurant: Restaurant;
  menu:Food[];
  name:string;
  id:string;
  myBackgroundImageUrl = 'assets/menu.png';

  constructor(private router: ActivatedRoute, private dataService: DataService,private route: Router) { }

  ngOnInit() {
    this.loadRestaurant();
 
    }
   

    loadRestaurant(): void {
    this.router.params.subscribe(
      (params: Params) => {
        let Name: string = params["Name"];  
        let City: string = params["City"];  
        
        this.dataService.getRestaurantByNameCity(Name,City).subscribe(
          (data: Restaurant[]) => {
    
            this.restaurant = data[0]; 
             
            this.Menu();
          }
        );
      }
    );
  }
  Menu():void
  {
    this.dataService.getMenu(this.restaurant.Name,this.restaurant.City).subscribe(
      (data: Food[]) => {
        this.menu = data;
      }
    );
  }

  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }
}
