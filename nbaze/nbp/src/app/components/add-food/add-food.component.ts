import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { Restaurant } from 'src/app/models/restaurant.model';

@Component({
  selector: 'app-add-food',
  templateUrl: './add-food.component.html',
  styleUrls: ['./add-food.component.scss']
})
export class AddFoodComponent implements OnInit {
  name: string;
  type: string;
  restaurants:Restaurant[];
  selectedRestaurant:Restaurant;
  PictureURL:string;
  description:string;
  user:User;
  myBackgroundImageUrl = 'assets/pr.png';
 

  @ViewChild('form',{static: false})
  form: NgForm; 

  constructor(private dataService: DataService,private router: Router) { 
    this.loadRestaurants();

  }

  ngOnInit() {
   this.user=new User();
    this.user.Username=localStorage.getItem("Username");

  }
  test()
  {
    
    let a:string = this.selectedRestaurant.Name;
    console.log(this.selectedRestaurant);
    
  }

  addFood() {
    let name: string = this.name;
     let type: string = this.type;
     let description:string=this.description;
     let restaurantName:string=this.selectedRestaurant.Name;
     let RestaurantCity:string=this.selectedRestaurant.City;
     const pictureURL = this.PictureURL; 
     const username=this.user.Username;

    this.dataService.addFood(username,name,type,description,restaurantName,RestaurantCity,pictureURL).subscribe(
      (success: any) => {
        if(!success){
          return;
          
        }
       // this.linkToFood(name, pictureURL);
        //this.router.navigate(['home']);
      }
    );
    this.form.reset();
    this.router.navigate(['home']);

  }

  loadRestaurants(): void {
    this.dataService.getAllRestaurants().subscribe(
      (data: Restaurant[]) => {
        this.restaurants = data;
      }
    );
  }
  addRestaurant()
  {
        this.router.navigate(['addRestaurant']);

  }

  linkToFood(id: string, pictureURL: string)
  {
    this.dataService.link(id,pictureURL).subscribe(
      (success: any) => {
        if(!success){
          return;
          
        }
       // this.router.navigate(['home']);
      }
    );
   // this.form.reset();
  }
 
  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }

}
