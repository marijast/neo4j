import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CreateProfileComponent } from '../create-profile/create-profile.component';
import { AddFoodComponent } from '../add-food/add-food.component';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
 // router: any;

  constructor(public dialog: MatDialog,private dataService: DataService,private router: Router) { }

  ngOnInit() {
  }

  

  openDialogAF(): void {
    const dialogRef = this.dialog.open( AddFoodComponent, {
      width: '400px',
      height:'400px'
      
    });
  
  dialogRef.afterClosed().subscribe(result => {
    
  });
}
isLogged(): boolean {
  return this.dataService.isUserLogged();
}

logOut(): void {
  localStorage.removeItem("Username");
  localStorage.removeItem("Password");
  this.router.navigate(['home']);
}
}
