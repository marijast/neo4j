import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { NgForm } from '@angular/forms';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-create-profile',
  templateUrl: './create-profile.component.html',
  styleUrls: ['./create-profile.component.scss']
})
export class CreateProfileComponent implements OnInit {
  myBackgroundImageUrl = 'assets/pr.png';

  name: string;
  username: string;
  user: User;
  @ViewChild('form',{static: false})
  registerForm: NgForm;
  

  constructor(private dataService: DataService, private router: Router,private snackBar: MatSnackBar) {
    this.user = new User();
   }

  ngOnInit() {
  }
  onSubmit() {
    let name: string = this.user.Name;
   // let name: string = this.name;
     let username: string = this.user.Username;
     let password: string = this.user.Password;
    // let username: string = this.username;
    this.dataService.register(name,username,password).subscribe(
      (success: boolean) => {
        if(!success){
        return;
          
        }
        this.router.navigate(['logIn']);
      },
      (error) => {
        //neuspesna reg
       //console.log(error);
       let message = "Neuspešna registracija.Korisničko ime koje ste uneli već postoji";
       this.snackBar.open(message, "Zatvori", {
         duration: 2000,
       });
     }
    );

    this.registerForm.reset();
  }
  
 

  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }

}
