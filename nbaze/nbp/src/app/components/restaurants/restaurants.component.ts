import { Component, OnInit, ViewChild } from '@angular/core';
import { Restaurant } from 'src/app/models/restaurant.model';
import { DataService } from 'src/app/service/data.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.scss']
})
export class RestaurantsComponent implements OnInit {

  City:string;
  restaurants: Restaurant[];
  myBackgroundImageUrl = 'assets/rest.png';

  @ViewChild('form',{static: false})
  form: NgForm; 

  constructor(private dataService: DataService,private router: Router) { }

    ngOnInit() {
    this.loadRestaurants();
  }

  loadRestaurants(): void {
    this.dataService.getAllRestaurants().subscribe(
      (data: Restaurant[]) => {
        this.restaurants = data;
      }
    );
  }

  find(): void {
    let City: string = this.City;
    this.dataService.getRestaurantByCity(City).subscribe(
      (data: Restaurant[]) => {
        this.restaurants=data;
           });
       // this.loadFood();
  
  }

  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }
 


}
