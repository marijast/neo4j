import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TastedcardComponent } from './tastedcard.component';

describe('TastedcardComponent', () => {
  let component: TastedcardComponent;
  let fixture: ComponentFixture<TastedcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TastedcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TastedcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
